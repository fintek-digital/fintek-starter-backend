const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const helmet = require('helmet');
const sessions = require('express-session');
const routes = require('../api/routes/v1');
const sessionOptions = require('./session');
const { log, session } = require('./vars');
const error = require('../api/middlewares/error');
const rateLimiter = require('../api/middlewares/rateLimiter');
const swaggerDocs = require('./swagger');
const { corsOptions } = require('./cors');

/**
* Express instance
* @public
*/
const app = express();

// enable session management
app.set('trust proxy', session.trustProxy);
app.use(sessions(sessionOptions));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Methods', 'GET,POST');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
});

// request logging. dev: console | production: file
app.use(morgan(log.name));

// parse body params and attach them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// gzip compression
app.use(compress());

// lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors(corsOptions));

// secure apps by limit the call rate
app.use(rateLimiter);

// mount swagger docs routes
swaggerDocs(app);

// mount api v1 routes
app.use('/v1', routes);

// if error is not an instanceOf APIError, convert it.
app.use(error.converter);

// catch 404 and forward to error handler
app.use(error.notFound);

// error handler, send stacktrace only during development
app.use(error.handler);

module.exports = app;
