/* eslint-disable class-methods-use-this */
const Transport = require('winston-transport');
const axiosClient = require('axios');
const { log, axios } = require('./vars');

class LogService extends Transport {
  constructor(opts) {
    super(opts);
    this.name = 'logservice';
    this.options = opts;
    this.client = axiosClient.create({
      baseURL: log.baseUrl,
      timeout: axios.timeout,
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
        'Accept-Encoding': 'gzip, deflate, br',
        Connection: 'keep-alive',
        Accept: '*/*',
      },
    });
  }

  processData(data = {}, keys = []) {
    const result = {};
    const dataCopy = { ...data };

    keys.forEach((key) => {
      const val = dataCopy[key] ?? null;
      result[key] = val;
      delete dataCopy[key];
    });

    return {
      parsedData: dataCopy,
      ...result,
    };
  }

  log(info, callback) {
    const { level, message, ...winstonMeta } = info;
    const defaultMeta = this.options.defaultMeta || {};
    const data = { ...defaultMeta, ...winstonMeta };

    const {
      parsedData,
      label,
      rid,
      applicationName,
    } = this.processData(data, ['label', 'rid', 'applicationName', 'id']);

    process.nextTick(async () => {
      try {
        await this.client({
          method: 'post',
          url: '/v1/logs',
          data: {
            message,
            label,
            level,
            data: parsedData,
            applicationName, // TODO: change this to serviceName in case this is a service
            rid,
          },
        });

        setImmediate(() => {
          this.emit('logged', info);
        });
      } catch (error) {
        setImmediate(() => {
          this.emit('error', error);
        });
      }
    });

    callback();
  }
}

module.exports = LogService;
