const { allowedOrigins } = require('./vars');

module.exports = {
  corsOptions: {
    origin: allowedOrigins,
    credentials: true,
    allowedHeaders: ['Content-Type', 'Authorization'],
  },
};
