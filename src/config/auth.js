const jsonwebtoken = require('jsonwebtoken');
const AES = require('crypto-js/aes');
const { jwt } = require('./vars');

/**
 * Get user with custom logic
 * Insert your custom logic here to test the user
 * @param {object} payload
 */
const getUser = async (payload) => {
  const {
    username,
    password,
  } = payload;

  let user = null;

  // THIS LOGIC IS ONLY FOR EXAMPLE
  // TODO: CHANGE THIS IN PRODUCTION!!
  if (username === 'admin' && password === '1234') {
    user = {
      firstName: 'ישראל',
      lastname: 'ישראלי',
      email: 'israel@example.com',
      id: '1233',
    };
  }

  return user;
};

/**
 * Create jwt token with encoded data
 * @param {object} data
 * @returns token
 */
const createToken = (data = {}) => {
  const encoded = AES.encrypt(
    JSON.stringify(data),
    jwt.cryptoSecret,
  ).toString();

  const token = jsonwebtoken.sign(
    { data: encoded },
    jwt.secret,
    { expiresIn: parseInt(jwt.seconds, 10) },
  );

  return token;
};

module.exports = {
  getUser,
  createToken,
};
