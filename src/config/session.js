const sessions = require('express-session');
const MemoryStore = require('memorystore')(sessions);
const { session, name } = require('./vars');

const sessionOptions = {
  name,
  secret: session.secret,
  resave: false,
  saveUninitialized: true,
  store: new MemoryStore({
    checkPeriod: session.maxage,
  }),
  cookie: {
    path: session.path,
    httpOnly: session.httpOnly,
    secure: session.secure,
    maxAge: session.maxage,
    domain: session.domain,
    sameSite: session.sameSite,
  },
};

module.exports = sessionOptions;
