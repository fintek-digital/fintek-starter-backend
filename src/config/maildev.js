const MailDev = require('maildev');
const { env } = require('./vars');

/**
* SMTP server
* @public
*/
module.exports = {
  listen: () => {
    if (env === 'development') {
      const maildev = new MailDev();
      maildev.listen();
    }
  },
};
