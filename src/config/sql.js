const mssql = require('mssql');
const { sql } = require('./vars');

const pools = new Map();
const connections = {
  main: `Server=${String(sql.server)},${+sql.port};Database=${sql.database};User Id=${sql.user};Password=${sql.password};Encrypt=false`,
};

const sqlTypes = {
  bit: mssql.Bit,
  bigint: mssql.BigInt,
  decimal: mssql.Decimal,
  float: mssql.Float,
  int: mssql.Int,
  money: mssql.Money,
  numeric: mssql.Numeric,
  smallint: mssql.SmallInt,
  smallmoney: mssql.SmallMoney,
  real: mssql.Real,
  tinyint: mssql.TinyInt,
  char: mssql.Char,
  nchar: mssql.NChar,
  text: mssql.Text,
  ntext: mssql.NText,
  varchar: mssql.VarChar,
  nvarchar: mssql.NVarChar,
  xml: mssql.Xml,
  time: mssql.Time,
  date: mssql.Date,
  datetime: mssql.DateTime,
  datetime2: mssql.DateTime2,
  datetimeoffset: mssql.DateTimeOffset,
  smalldatetime: mssql.SmallDateTime,
  uniqueidentifier: mssql.UniqueIdentifier,
  variant: mssql.Variant,
  binary: mssql.Binary,
  varbinary: mssql.VarBinary,
  image: mssql.Image,
  udt: mssql.UDT,
  geography: mssql.Geography,
  geometry: mssql.Geometry,
};

module.exports = {
  /**
  * Get or create a pool. If a pool doesn't exist the config must be provided.
  * If the pool does exist the config is ignored (even if it was different to the one provided
  * when creating the pool)
  *
  * @param {string} name
  * @param {{}} [config]
  * @return {Promise.<mssql.ConnectionPool>}
  */
  get: (name) => {
    if (!pools.has(name)) {
      const config = (!name) ? connections.main : connections[name];

      if (!config) {
        throw new Error('Pool does not exist');
      }

      const pool = new mssql.ConnectionPool(config);
      // automatically remove the pool from the cache if `pool.close()` is called
      const close = pool.close.bind(pool);
      pool.close = (...args) => {
        pools.delete(name);
        return close(...args);
      };

      pools.set(name, pool.connect());
    }
    return pools.get(name);
  },

  /**
  * Closes all the pools and removes them from the store
  *
  * @return {Promise<mssql.ConnectionPool[]>}
  */
  closeAll: () => Promise.all(Array.from(pools.values())
    .map((connect) => connect.then((pool) => pool.close()))),

  types: (name) => {
    if (!name || !sqlTypes[name.toLowerCase()]) {
      throw new Error(`sql type '${name.toLowerCase()}' not exists`);
    }

    return sqlTypes[name.toLowerCase()];
  },
};
