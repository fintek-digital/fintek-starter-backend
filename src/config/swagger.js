const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const { port, env } = require('./vars');
const { name, version, description } = require('../../package.json');

const options = {
  definition: {
    openapi: '3.0.3',
    info: {
      title: name,
      description,
      version,
    },
    servers: [
      {
        url: `http://localhost:${port}/`,
        description: `${env} server`,
      },
    ],
    basePath: '/v1',
  },
  apis: ['./swagger/*.yaml'],
};

/**
 * Create urls for api docs & api docs as json
 * @param {object} app
 * @param {object} paths { docs, json }
 */
const swaggerDocs = (app, paths = { docs: '/docs', json: '/docs.json' }) => {
  if (env !== 'production') {
    const swaggerSpec = swaggerJsdoc(options);

    app.use(paths.docs, swaggerUi.serve, swaggerUi.setup(swaggerSpec));
    app.get(paths.json, (req, res) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(swaggerSpec);
    });
  }
};

module.exports = swaggerDocs;
