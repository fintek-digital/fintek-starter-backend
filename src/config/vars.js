const path = require('path');

// import .env variables on development
if (!process.env.NODE_ENV) {
  require('dotenv-safe').config({ // eslint-disable-line global-require
    path: path.join(__dirname, '../../.env'),
    example: path.join(__dirname, '../../.env.example'),
  });
}

/**
 * Change string to camel case
 * @param {string} str
 * @returns {object}
 * @private
 */
const toCamelCase = (str) => str.trim().replace(/[-_\s]+(.)?/g, (_, c) => (c ? c.toUpperCase() : ''));

/**
 * Get custom keys from process env and turn them into object
 * @returns {object}
 * @private;
 */
const getCustomObject = () => {
  const envKeys = Object.entries(process.env);
  const customEnv = envKeys.filter((item) => item[0].startsWith('CUSTOM_'));
  const customObj = {};
  customEnv.forEach(([key, value]) => {
    customObj[toCamelCase(key.replace('CUSTOM_', '').toLowerCase())] = value;
  });

  return customObj;
};

module.exports = {
  env: process.env.NODE_ENV,
  name: process.env.NAME,
  port: process.env.PORT,
  allowedOrigins: String(process.env.ALLOWED_ORIGINS).split(','),
  log: {
    name: (process.env.NODE_ENV === 'production') ? 'combined' : 'dev',
  },
  rateLimit: {
    windowMS: process.env.RATE_LIMITER_WINDOWMS,
    max: process.env.RATE_LIMITER_MAX,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    cryptoSecret: process.env.JWT_CRYPTO_SECRET,
    seconds: parseInt(process.env.JWT_EXPIRATION_SECONDS, 10),
  },
  sql: {
    port: process.env.SQL_PORT,
    user: process.env.SQL_USER,
    password: process.env.SQL_PASSWORD,
    database: process.env.SQL_DATABASE,
    server: process.env.SQL_SERVER,
  },
  axios: {
    baseUrl: process.env.AXIOS_BASEURL,
    timeout: process.env.AXIOS_TIMEOUT,
  },
  mailer: {
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    secure: process.env.MAILER_SECURE === 'true', // true for 465, false for other ports
    user: process.env.MAILER_USER,
    pass: process.env.MAILER_PASS,
    from: process.env.MAILER_FROM,
    to: process.env.MAILER_TO,
  },
  session: {
    secure: process.env.SESSION_SECURE === 'true',
    secret: process.env.SESSION_SECRET,
    trustProxy: parseInt(process.env.SESSION_TRUST_PROXY, 10),
    maxage: parseInt(process.env.SESSION_COOKIE_MAXAGE, 10),
    path: process.env.SESSION_COOKIE_PATH,
    httpOnly: process.env.SESSION_COOKIE_HTTP_ONLY === 'true',
    domain: process.env.SESSION_COOKIE_DOMAIN,
    sameSite: process.env.SESSION_COOKIE_SAME_SITE === 'true',
  },
  custom: {
    ...getCustomObject(),
  },
};
