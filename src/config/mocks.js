/* eslint-disable no-console */
const msw = require('msw/node');
const handlers = require('../api/mocks');
const { env } = require('./vars');

const printHandlers = (routes = []) => {
  routes.forEach((handler) => {
    const {
      method,
      path,
      callFrame,
      operationType,
    } = handler.info;

    const pragma = operationType ? '[graphql]' : '[rest]';

    console.log('\x1b[36m%s\x1b[0m', `${pragma} ${method} ${path}`);
    console.log('\x1b[36m%s\x1b[0m', `Declaration: ${callFrame}\n`);
  });
};

/**
* Mocks server
* @public
*/
module.exports = {
  start: () => {
    if (env === 'development') {
      const server = msw.setupServer(...handlers);
      printHandlers(server.listHandlers());

      server.events.on('request:match', ({ request }) => {
        console.log('\x1b[36m%s\x1b[0m', `request mock match ${request.method} ${request.url}`);
      });

      server.events.on('request:unhandled', ({ request }) => {
        console.log('\x1b[36m%s\x1b[0m', `request mock unhandled ${request.method} ${request.url}`);
      });

      server.events.on('unhandledException', ({ request, requestId, error }) => {
        console.log('%s %s errored! See details below.', request.method, request.url, requestId);
        console.error(error);
      });

      server.listen();
    }
  },
};
