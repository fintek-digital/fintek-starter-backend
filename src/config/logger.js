const winston = require('winston');
const SqlService = require('./winston-logservice-transport');
const { LOGGER_LABELS } = require('../api/utils/enums');

const transporterConfig = {
  defaultMeta: {
    rid: '0',
    label: LOGGER_LABELS.misc,
  },
};

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

//
// If we're not in development than log to sql
//
if (process.env.NODE_ENV !== 'development') {
  logger.add(new SqlService(transporterConfig));
}

logger.stream = {
  write: (message) => {
    logger.info(message.trim());
  },
};

module.exports = logger;
