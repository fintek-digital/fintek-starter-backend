const { port, env } = require('./config/vars');
const logger = require('./config/logger');
const app = require('./config/express');
const mocks = require('./config/mocks');
const maildev = require('./config/maildev');

// start mocks server
mocks.start();

// start the smtp mail server
maildev.listen();

// listen to requests
app.listen(port, () => logger.info(`server started on port ${port} (${env})`));

/**
* Exports express
* @public
*/
module.exports = app;
