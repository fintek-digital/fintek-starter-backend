const DEFAULT_ERROR = { code: 0, label: '', message: '' };

/**
 * Class representing a response
 * @public
 */
class Response {
  constructor(data = {}, object = null) {
    this.createdAt = new Date().getTime();
    this.error = DEFAULT_ERROR;

    this.object = object || getObjectName('app');

    if (data.error) {
      this.error = {
        ...this.error,
        ...data.error,
      };
    }

    if (typeof data === 'object') {
      delete data.object; // eslint-disable-line
      delete data.createdAt; // eslint-disable-line
      this.success = !(this.error?.code > 0);
      Object.assign(this, data);
    }
  }
}

/**
 * get object name by raising error and get caller function name
 * @param {string} object default value
 * @returns {string} object name
 */
const getObjectName = (object) => {
  try {
    const logLineDetails = ((new Error().stack).split('at ')[3]).trim();
    const functionName = logLineDetails.split(' ')[0].split('.');
    let objectName = functionName[functionName.length - 1].toLowerCase();

    if (objectName.includes(':')) {
      const paths = functionName[0].toLowerCase().split('/');
      objectName = paths[paths.length - 1].toLowerCase();
    }

    return objectName;
  } catch (e) {
    return object;
  }
};

module.exports = Response;
