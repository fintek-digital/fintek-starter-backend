const Joi = require('joi');

module.exports = {

  // POST /v1/example/query/:id
  getPolicyValidation: {
    body: Joi.object({
      id: Joi.number().min(1).required(),
    }),
  },
};
