const express = require('express');
const httpStatus = require('http-status');
const authRoutes = require('./auth.route');
const exampleRoutes = require('./example.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.json({
  code: httpStatus.OK,
  message: 'ok',
}));

/**
 * METHOD v1/auth
 */
router.use('/auth', authRoutes);

/**
 * METHOD v1/example
 */
router.use('/example', exampleRoutes);

module.exports = router;
