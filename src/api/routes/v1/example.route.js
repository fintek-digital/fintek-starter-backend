const express = require('express');
const { validate } = require('express-validation');
const controller = require('../../controllers/example.controller');
const { getPolicyValidation } = require('../../validations/example.validation');
const authenticate = require('../../middlewares/authenticate');

const router = express.Router();

router
  .route('/send')
  .get(controller.sendPolicy);

router
  .route('/log')
  .get(controller.log);

router
  .route('/get')
  .post(
    validate(getPolicyValidation),
    controller.getPolicy,
  );

router
  .route('/session')
  .get(controller.session);

router
  .route('/profile')
  .get(
    authenticate,
    controller.profile,
  );

module.exports = router;
