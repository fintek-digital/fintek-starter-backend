/* eslint-disable no-param-reassign */
const axiosClient = require('axios');
const { v4: uuidv4 } = require('uuid');
const logger = require('../../config/logger');
const { axios } = require('../../config/vars');
const { LOGGER_LABELS } = require('./enums');
const { name: applicationName } = require('../../../package.json');

const getClient = (options = {}) => {
  const {
    type = 'xml',
    url,
    timeout = axios.timeout,
    headers = {},
  } = options;

  const baseURL = url || axios.baseUrl;

  const clientHeaders = {
    'Content-Type': (type === 'json') ? 'application/json' : 'text/xml;charset=utf-8',
    'Accept-Encoding': 'gzip, deflate, br',
    Connection: 'keep-alive',
    Accept: '*/*',
    ...headers,
  };

  const client = axiosClient.create({
    baseURL,
    timeout,
    headers: clientHeaders,
  });

  // add interceptors
  client.interceptors.request.use((config) => {
    config.metadata = config.metadata || {};
    config.metadata.requestTime = new Date().getTime();
    config.metadata.uid = uuidv4();

    // if url starts with url prefix then remove our baseurl set in env
    const pattern = /^((http|https|ftp):\/\/)/;
    if (pattern.test(config.url)) {
      config.baseURL = '';
    }

    logger.info(
      `${config.method} request made to ${config.baseURL}${config.url}`,
      {
        uid: config.metadata.uid,
        label: LOGGER_LABELS.request,
        method: config.method,
        url: `${config.baseURL}${config.url}`,
        requestTime: config.metadata.requestTime,
        data: config.data,
        rid: config.data.rid,
        applicationName, // TODO: change this to serviceName in case this is a service
      },
    );

    return config;
  }, (error) => Promise.reject(error));

  client.interceptors.response.use((response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    const { metadata } = response.config;
    metadata.responseTime = new Date().getTime();
    metadata.durationTime = metadata.responseTime - metadata.requestTime;
    const fullUrl = `${response.config.baseURL}${response.config.url || ''}`;

    logger.info(
      `${response.config.method} response from ${fullUrl}`,
      {
        uid: metadata.uid,
        label: LOGGER_LABELS.response,
        responseTime: metadata.responseTime,
        url,
        durationTime: metadata.durationTime,
        data: response.data,
        rid: metadata.rid,
        applicationName, // TODO: change this to serviceName in case this is a service
      },
    );

    return response;
  }, (error) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    const responseTime = new Date().getTime();
    const requestTime = error.config?.metadata?.requestTime;
    const durationTime = (requestTime) ? responseTime - requestTime : 0;
    const fullUrl = `${error.config.baseURL}${error.config.url || ''}`;

    logger.error(
      `${error.code}: ${error.config.method} response from ${fullUrl}`,
      {
        uid: error.config.metadata.uid,
        label: LOGGER_LABELS.error,
        responseTime,
        url,
        durationTime,
        rid: error.config?.metadata?.rid,
        applicationName, // TODO: change this to serviceName in case this is a service
      },
    );

    return Promise.reject(error);
  });

  return client;
};

module.exports = getClient;
