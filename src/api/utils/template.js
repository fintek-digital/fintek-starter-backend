/**
 * update text, replace placeholders with data
 * @param {string}  source text source
 * @param {object}  data object of key/value
 * @param {object}  options
 * @returns changed string
 */
const updatePlaceholders = (source = '', data = {}, options = {}) => {
  const opts = {
    openTag: '{', // placeholder open tag
    closeTag: '}', // placeholder close tag
    deleteNoMatch: true, // delete placeholder if no key in data that match placeholder
    searchGlobal: true, // search placeholder only once or global
    ...options,
  };

  const find = `${opts.openTag}\\w+${opts.closeTag}`;
  const regex = new RegExp(find, opts.searchGlobal ? 'g' : '');

  return source.replace(regex, (key) => {
    const dataKey = key.replace(opts.openTag, '').replace(opts.closeTag, '');
    return data[dataKey] || (opts.deleteNoMatch ? '' : key);
  });
};

/**
 * update text, replace `if` placeholders when eneded
 * @param {string} source text source
 * @param {object} data object of data
 * @param {object} options
 * @returns changed string
 */
const updateTextIf = (source = '', data = {}, options = {}) => {
  const opts = {
    openIfTagStart: '{#if ', // open tag start
    openIfTagEnd: '}', // open tag end
    closeIfTag: '{\\/if}',
    deleteNoMatch: true, // delete placeholder if no key in data that match placeholder
    searchGlobal: true, // search placeholder only once or global
    zeroBasedIndex: false, // start the index count with 0 or 1
    ...options,
  };

  let output = source;
  const findIfWord = `${opts.openIfTagStart}(\\w+)${opts.openIfTagEnd}([\\s\\S]*?)${opts.closeIfTag}`;
  const regex = new RegExp(findIfWord, opts.searchGlobal ? 'g' : '');
  let matches = regex.exec(source);

  while (matches !== null) {
    const originalMatch = matches[0];
    const tester = data[matches[1]] || null;
    const str = matches[2] || '';

    const replaceWith = (!tester) ? '' : str.trim();
    output = output.replace(originalMatch, replaceWith);

    matches = regex.exec(output);
  }

  return output;
};

/**
 * update text, replace placeholders inside loops with data
 * @param {string} source text source
 * @param {object} data data of key/value
 * @param {object} options
 * @returns changed string
 */
const updateTextLoops = (source = '', data = {}, options = {}) => {
  const opts = {
    openTagStart: '{#each ', // open tag start
    openTagEnd: '}', // open tag end
    closeTag: '{\\/each}',
    deleteNoMatch: true, // delete placeholder if no key in data that match placeholder
    searchGlobal: true, // search placeholder only once or global
    zeroBasedIndex: false, // start the index count with 0 or 1
    ...options,
  };

  let output = source;
  const findLoopWord = `${opts.openTagStart}(\\w+)${opts.openTagEnd}([\\s\\S]*?)${opts.closeTag}`;
  const regex = new RegExp(findLoopWord, opts.searchGlobal ? 'g' : '');
  let matches = regex.exec(source);

  while (matches !== null) {
    if (matches.length === 3) {
      const originalString = matches[0];
      const arr = data[matches[1]] || [];
      const str = matches[2];
      let replaceString = '';

      for (let index = 0; index < arr.length; index += 1) {
        replaceString += updatePlaceholders(
          str,
          { ...arr[index], index: (opts.zeroBasedIndex) ? index : index + 1 },
        );
      }
      output = output.replace(originalString, replaceString);
    }
    matches = regex.exec(source);
  }

  return output;
};

/**
 * Minimal string templating (loops and placeholders)
 * @param {string} source source text
 * @param {object} data
 * @param {object} options
 * @returns changed string
 */
const textTemplate = (text, data = {}, options = {}) => {
  let output = text;

  if (!output) {
    return null;
  }

  // work on if exists
  output = updateTextIf(output, data, options);

  // work on loops
  output = updateTextLoops(output, data, options);

  // work on placeholders
  output = updatePlaceholders(output, data, options);

  return output;
};

module.exports = {
  textTemplate,
};
