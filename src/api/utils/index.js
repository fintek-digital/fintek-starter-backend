const fs = require('fs').promises;
const { axios } = require('../../config/vars');

/**
 * Attach baseurl to path
 * @param {string} pathUrl
 * @returns string
 */
const withBaseUrl = (pathUrl = '') => `${axios.baseUrl}${pathUrl}`;

/**
 * Get file from data folder
 * @param {string} filename
 * @returns {string}
 */
const getDataFile = async (filename = '') => {
  try {
    const output = await fs.readFile(`${__dirname}/../../data/${filename}`);
    return output.toString();
  } catch {
    return null;
  }
};

module.exports = {
  withBaseUrl,
  getDataFile,
};
