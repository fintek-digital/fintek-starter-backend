const ERROR = Object.freeze({
  unknown: {
    code: 30,
    label: 'unknown error',
    message: 'שגיאה לא ידועה',
  },
  alreadyLogged: {
    code: 110,
    label: 'already logged',
    message: 'המשתמש מחובר',
  },
  loginFailed: {
    code: 111,
    label: 'login failed',
    message: 'לא ניתן להתחבר עם הפרטים שהוזנו',
  },
});

const LOGGER_LABELS = Object.freeze({
  misc: 'MISC',
  request: 'REQUEST',
  response: 'RESPONSE',
  error: 'ERROR',
  server: 'SERVER',
  policy: 'POLICY',
  lead: 'LEAD',
  file: 'FILE',
  email: 'EMAIL',
  creditGuard: 'CREDITGUARD',
  token: 'TOKEN',
  database: 'DATABASE',
  webService: 'WEBSERVICE',
});

module.exports = {
  ERROR,
  LOGGER_LABELS,
};
