const xmljs = require('xml-js');

/**
 * convert xml document into json object
 * @param {string} xml
 * @returns {object}
 */
const xml2json = (xml) => {
  try {
    const result = xmljs.xml2json(xml, {
      compact: true,
      spaces: 5,
      ignoreDoctype: true,
      attributesKey: 'attributes',
    });

    return JSON.parse(result);
  } catch (error) {
    return {};
  }
};

/**
 * conver string to number
 * @param {string} val
 * @returns {number}
 */
const string2number = (val) => {
  if (val === undefined || val === null) return null;

  return (Number.isNaN(+val)) ? val : +val;
};

module.exports = {
  xml2json,
  string2number,
};
