/**
 * helper function to check if string includes all strings inside
 * @param {string} source
 * @param {array} data
 * @returns {boolean}
 */
const includesAll = (source = '', data = []) => {
  let arr = [];
  if (typeof data === 'string' || data instanceof String) {
    arr = data.split();
  } else if (Array.isArray(data)) {
    arr = data;
  }

  return arr.every((element) => source.includes(element));
};

module.exports = {
  includesAll,
};
