/**
 * check if variable is empty object
 * @param {mixed} obj
 * @returns {boolean}
 */
const isObjectEmpty = (obj = {}) => {
  try {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  } catch (error) {
    return false;
  }
};

/**
   * check if variable is array
   * @param {mixed} val
   * @returns {boolean}
   */
const isArray = (val) => Array.isArray(val);

/**
   * check if variable is string
   * @param {mixed} val
   * @returns {boolean}
   */
const isString = (val) => (typeof val === 'string' || val instanceof String);

/**
   * check if variable is boolean
   * @param {mixed} val
   * @returns {boolean}
   */
const isBoolean = (val) => (typeof val === 'boolean');

/**
   * check if variable is object
   * @param {mixed} val
   * @returns {boolean}
   */
const isObject = (val) => Object.prototype.toString.call(val) === '[object Object]';

/**
   * check if variable is date object
   * @param {mixed} val
   * @returns {boolean}
   */
const isDateObject = (val) => (val instanceof Date);

/**
   * check if variable is empty
   * @param {mixed} val
   * @returns {boolean}
   */
const isEmpty = (val) => {
  if (isString(val) && String(val).length) { return false; }
  if (isBoolean(val) && val) { return false; }
  if (isArray(val) && val.length) { return false; }
  if (!isObjectEmpty(val)) { return false; }

  return true;
};

/**
   * check if variable is function
   * @param {mixed} val
   * @returns {boolean}
   */
const isFunction = (val) => (Object.prototype.toString.call(val) === '[object Function]');

/**
   * check if variable is number
   * @param {mixed} val
   * @returns {boolean}
   */
const isNumber = (val) => Number.isInteger(val);

module.exports = {
  isObjectEmpty,
  isArray,
  isString,
  isBoolean,
  isObject,
  isDateObject,
  isEmpty,
  isFunction,
  isNumber,
};
