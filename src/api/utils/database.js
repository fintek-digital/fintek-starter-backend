const logger = require('../../config/logger');
const { get, sqlTypes } = require('../../config/sql');
const { LOGGER_LABELS } = require('./enums');
const { isObject, isString } = require('./is');

/**
 * helper function to log error for stored procedure
 * @param {string} storedProcedure
 * @param {object} data
 * @private
 */
const logError = (storedProcedure = '', data = {}) => {
  let logData = { label: LOGGER_LABELS.database };

  if (isObject(data)) {
    logData = { ...logData, ...data };
  } else if (isString(data)) {
    logData = { ...logData, data };
  }

  const spName = (isString(storedProcedure) && storedProcedure.length) ? storedProcedure : '';

  logger.error(`failure in ${spName} stored procedure`, logData);
};

/**
 * get parameters
 * @returns {object} database result
 */
const getParamsQuery = async () => {
  const storedProcedure = 'readParameters';

  try {
    const pool = await get();
    const result = await pool.request().execute(storedProcedure);

    return (result.recordsets[0].length === 1) ? result.recordset[0] : null;
  } catch (error) {
    logError(storedProcedure, { error });

    return null;
  }
};

/**
 * get countries
 * @param {object} data
 * @returns {array} countries
 */
const getCountriesQuery = async (data = {}) => {
  const {
    title = '',
    continent = '',
  } = data;

  const storedProcedure = 'readCountries';
  const countries = [];

  try {
    const pool = await get();
    const result = await pool.request()
      .input('title', sqlTypes.NVarChar, title)
      .input('continent', sqlTypes.NVarChar, continent)
      .execute(storedProcedure);

    if (result.recordsets[0].length) {
      result.recordset.forEach((row) => {
        countries.push({
          id: row.countryId,
          title: row.countryTitle,
          continent: row.continentTitle,
        });
      });
    }
  } catch (error) {
    logError(storedProcedure, { data, error });
  }

  return countries;
};

module.exports = {
  getParamsQuery,
  getCountriesQuery,
};
