module.exports = `
    {
      "header": {
        "USR_TYPE": 12,
        "USR_NAME": "LORINE",
        "SESSION_ID": "3639c606-7ae7-41d7-ab03-",
        "LOB": 110
      },
      "data": {
        "DaysInAdvance": 180.0,
        "ValidAge": 80.0,
        "TotalAllowedDays": 365.0,
        "ExtremeSportValidAge": 80.0,
        "WinterSportValidAge": 70.0,
        "CompetSportValidAge": 80.0,
        "PregnancyValidAgeFrom": 16.0,
        "PregnancyValidAgeUpto": 48.0,
        "DaysForReturnCustoer": 180.0,
        "LaptopLimit": 1500.0,
        "CelularLimit": 1000.0,
        "CameraLimit": 2000.0
      },
      "error": {
        "SYS_ERR_CODE": 0,
        "SYS_ERR_DESC": "",
        "APP_ERR_CODE": 0,
        "APP_ERR_DESC": ""
      }
    }
  `;
