const { HttpResponse } = require('msw');
const responseParams = require('./params');
const responseQuote = require('./quote');
const { includesAll } = require('../../utils/mock');

const mock = async ({ request }) => {
  const data = await request.body;
  const body = await new Response(data).text();

  if (includesAll(body, ['<LOB>669</LOB>', '<ACTIVITY>613</ACTIVITY>'])) {
    return HttpResponse.json(responseQuote);
  }

  if (includesAll(body, '"LOB": 110')) {
    return HttpResponse.json(responseParams);
  }

  return HttpResponse.json({ success: false });
};

module.exports = mock;
