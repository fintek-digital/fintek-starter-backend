const { http } = require('msw');
const handlerExample = require('./example');
const { withBaseUrl } = require('../utils');

module.exports = [
  http.post(withBaseUrl(), handlerExample),
];
