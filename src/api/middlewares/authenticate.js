const httpStatus = require('http-status');
const jsonwebtoken = require('jsonwebtoken');
const encUTF8 = require('crypto-js/enc-utf8');
const AES = require('crypto-js/aes');
const Response = require('../handlers/response');
const { jwt } = require('../../config/vars');

/**
 * Decode token data
 * @param {string} token
 * @returns decoded data
 */
const decodeToken = (token) => {
  try {
    const decoded = jsonwebtoken.verify(token, jwt.secret);
    const decryptedData = AES.decrypt(decoded.data, jwt.cryptoSecret);
    const data = JSON.parse(decryptedData.toString(encUTF8));
    return data;
  } catch (error) {
    return null;
  }
};

/**
 * Authenticate middleware. add to route to protect
 * @param {object} req
 * @param {object} res
 * @param {object} next
 */
const authenticate = (req, res, next) => {
  const {
    session,
  } = req;

  const bearerHeader = req.headers?.authorization;

  if (bearerHeader) {
    const bearer = bearerHeader.split(' ');
    const token = bearer[bearer.length - 1];

    if (token === session.accessToken) {
      const decoded = decodeToken(token);
      req.user = decoded;

      return next();
    }
  }

  const response = new Response({
    message: 'failed authentication',
    status: httpStatus.UNAUTHORIZED,
  });

  res.status(httpStatus.UNAUTHORIZED);
  return res.json(response);
};

module.exports = authenticate;
