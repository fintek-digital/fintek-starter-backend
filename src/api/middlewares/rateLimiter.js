const rateLimiter = require('express-rate-limit');
const httpStatus = require('http-status');
const { env, rateLimit } = require('../../config/vars');
const APIError = require('../handlers/error-api');

/**
 * Limit api calls. enable only on production
 * @public
 */
module.exports = rateLimiter({
  windowMs: rateLimit.windowMS, // window time in milisecconds
  max: rateLimit.max, // limit each IP to x requests per windowMs
  skip: () => (env !== 'production'),
  headers: true,
  handler: (req, res, next) => {
    const apiError = new APIError({
      message: 'Limit max reach',
      status: httpStatus.FORBIDDEN,
    });

    return next(apiError);
  },
});
