const httpStatus = require('http-status');
const expressValidation = require('express-validation');
const ErrorApi = require('../handlers/error-api');
const { env } = require('../../config/vars');

/**
 * get validation message from validation object error
 * @param {object} obj
 * @returns {string}
 * @private
 */
const getValidationMessage = (obj) => {
  const output = [];

  if (!obj) {
    return output;
  }

  Object.keys(obj).forEach((key) => {
    const val = obj[key];
    if (Array.isArray(val)) {
      output.push(`[${key}] ${val[0].message}`);
    }
  });

  return output;
};

/**
 * Error handler. Send stacktrace only during development
 * @public
 */
const handler = (err, req, res, next) => {
  const response = {
    success: false,
    status: err.status || err.statusCode,
    error: {
      code: err.code || 900,
      label: err.errors || 'Error handler',
      message: err.message || httpStatus[err.status || err.statusCode] || 'Error Message',
    },
    stack: err.stack,
  };

  if (env !== 'development') {
    delete response.stack;
  }

  res.status(err.status || err.statusCode);
  res.json(response);
};
exports.handler = handler;

/**
 * If error is not an instanceOf ErrorApi, convert it.
 * @public
 */
exports.converter = (err, req, res, next) => {
  let convertedError = err;

  if (err instanceof expressValidation.ValidationError) {
    convertedError = new ErrorApi({
      message: 'Validation Error',
      errors: getValidationMessage(err.details) || err.error,
      status: err.status || err.statusCode,
      stack: err.stack,
      code: 700,
    });
  } else if (!(err instanceof ErrorApi)) {
    convertedError = new ErrorApi({
      status: err.status || err.statusCode,
      message: err.message,
      stack: err.stack,
      code: 800,
    });
  }

  return handler(convertedError, req, res);
};

/**
 * Catch 404 and forward to error handler
 * @public
 */
exports.notFound = (req, res, next) => {
  const err = new ErrorApi({
    status: httpStatus.NOT_FOUND,
    message: 'Cant find route',
    errors: 'Not found',
    code: 404,
  });

  return handler(err, req, res);
};
