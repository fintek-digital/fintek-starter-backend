const debug = require('debug')('app');
const httpStatus = require('http-status');
const { getDataFile } = require('../utils');
const getAxiosClient = require('../utils/axios');
const mail = require('../../config/nodeMailer');
const logger = require('../../config/logger');
const Response = require('../handlers/response');

/**
 * Session is counting views
 * @public
 */
exports.session = async (req, res, next) => {
  try {
    const {
      session,
    } = req;

    session.views = (session.views) ? session.views += 1 : 1;
    const response = new Response({ views: session.views });

    res.status(httpStatus.OK);
    res.json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * User profile, available after login
 * @protected
 */
exports.profile = async (req, res, next) => {
  try {
    const {
      user,
    } = req;

    const response = new Response({ user });

    res.status(httpStatus.OK);
    res.json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Get policy from endpoint
 * @public
 */
exports.getPolicy = async (req, res, next) => {
  try {
    const {
      id,
    } = req.body;

    // prepare data
    const data = await getDataFile('example-xml.xml', {
      id,
      policy: '418247918274x',
    });

    const axiosClient = getAxiosClient();
    const result = await axiosClient.post('', data);
    // TODO: do something with result

    const response = new Response({ data: result.data });

    res.status(httpStatus.OK);
    res.json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Send policy to user
 * @public
 */
exports.sendPolicy = async (req, res, next) => {
  try {
    const html = await getDataFile('example-email.html');
    await mail.deliver(html.toString(), 'כותרת מייל');
    const response = new Response({ success: true });

    res.status(httpStatus.OK);
    res.json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Create log to console / sql
 * @public
 */
exports.log = (req, res, next) => {
  logger.info('test log', { user: 'israel' });
  logger.info('second luck');
  debug('debug test');
  const response = new Response({ ok: 4 });

  res.status(httpStatus.OK);
  res.json(response);
};
