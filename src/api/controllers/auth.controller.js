const httpStatus = require('http-status');
const Response = require('../handlers/response');
const { jwt } = require('../../config/vars');
const { getUser, createToken } = require('../../config/auth');
const { ERROR } = require('../utils/enums');

/**
 * Login user
 * @public
 */
// eslint-disable-next-line consistent-return
exports.login = async (req, res, next) => {
  try {
    const {
      session,
    } = req;

    if (session.accessToken) {
      res.status(httpStatus.UNAUTHORIZED);
      return res.json(new Response({ error: ERROR.alreadyLogged }));
    }

    const user = await getUser(req.body);

    if (user) {
      const t = new Date();
      const accessToken = createToken(user);
      const expires = {
        in: jwt.seconds,
        on: t.setSeconds(t.getSeconds() + jwt.seconds),
      };

      session.accessToken = accessToken;

      res.status(httpStatus.OK);
      return res.json(new Response({ accessToken, expires }));
    }

    res.status(httpStatus.UNAUTHORIZED);
    return res.json(new Response({ error: ERROR.loginFailed }));
  } catch (error) {
    next(error);
  }
};

/**
 * Logout user
 * @public
 */
exports.logout = (req, res, next) => {
  const {
    session,
  } = req;

  try {
    delete session.accessToken;

    res.status(httpStatus.OK);
    res.json(new Response());
  } catch (error) {
    next(error);
  }
};
