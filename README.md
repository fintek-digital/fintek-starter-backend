# Fintek Backend Express ES2017 REST API Boilerplate
Boilerplate/Generator/Starter Project for building RESTful APIs using Node.js, Express and SQL Server.
based on [express-rest-boilerplate](https://github.com/danielfsousa/express-rest-boilerplate)
  
## Requirements

 - [Node v7.6+](https://nodejs.org/en/download/current/)


## Running Locally

```bash
npm run dev
```

## Running in Production

```bash
npm start
```

## Lint

```bash
# lint code with ESLint
npm run lint
```

## Api Docs

```bash
# generate and open api documentation
npm run docs
```

## Documentation

This template come with built in support for alot of development options

### Axios

We use [axios](https://axios-http.com), a simple promise based HTTP client, to make all our endpoints calls.  

Each request and response automaticly logs calls.

### Secure endpoints

You can secure endpoint behind bearer header authentication token.  
In example.controller login function you can see how we created an access token
and returned it to the client after validation.  
now every route that has authenticate() function must recieve bearer header with that token
to be accessed.  

Token data available in controllers via req.tokendata.  

In middleware/auth.js **you must** set token validation or every token will be valid.  

### Logger

You should log every interaction with any part of your app using 
our [winston logger](https://github.com/winstonjs/winston).  

in production enviroment the logs are written into sql database.

### Mail

Mail send using [nodemailer](https://nodemailer.com/about/)  
and in development mode you can see the mail sent using 
the development smtp server installed in [http://localhost:1080](http://localhost:1080).  

Also we added the *deliver* function that give you the ability to send
emails fast and simple with the signature: deliver(html, subject, callback)

### Mocks

Mocks server [msw](https://mswjs.io) is activated in development enviroment 
and can help with mocking api calls to various endpoints.

### Sql

Connect to sql server using [mssql](https://www.npmjs.com/package/mssql) package.

### Variables

All process.env variables are available importing the vars.js file
from config folder.

Also, every enviroment that start with *CUSTOM_* will automaticly
be added to the vars object in the *custom* key with camelCase name.

in order to bebug mssql calls add mssql:base,mssql:tedi to DEBUG variable in env.

### Utilities

Global utilities are available in utils/index.js file.


## License

[ISC License](README.md) - [Fintek](https://fintek.co.il)
