# [1.3.0](https://bitbucket.org/fintek-digital/fintek-starter-backend/compare/v1.2.2...v1.3.0) (2025-02-06)


### Features

* add axios client and utility functions for XML and string templating ([357acd8](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/357acd8bbfd1f9003aebcbd28bfcc4a135d23dc5))

## [1.2.2](https://bitbucket.org/fintek-digital/fintek-starter-backend/compare/v1.2.1...v1.2.2) (2025-01-23)


### Bug Fixes

* update error to generic response ([5370d10](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/5370d10df02e83a2baa5d9931e31ce1c3d3010f6))

## [1.2.1](https://bitbucket.org/fintek-digital/fintek-starter-backend/compare/v1.2.0...v1.2.1) (2025-01-02)


### Bug Fixes

* add cookie security parameters ([bd5c5ff](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/bd5c5ff1f039702d162d66ae620d535b30bd09dd))
* remove restrict origin middleware ([9324a58](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/9324a5893c463e867801606ef697eaba78774150))

# [1.2.0](https://bitbucket.org/fintek-digital/fintek-starter-backend/compare/v1.1.0...v1.2.0) (2024-11-12)


### Bug Fixes

* remove apidoc ([af37ad0](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/af37ad0d705c4b294ddeccb8c35f6afe25f957a8))
* remove unneeded vars ([56e6daf](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/56e6daf2d07e34817a38743b46df41c6599523da))
* update mock ([0027446](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/00274463783933ab64d3cdac3fbf9ba2c40c4fd0))
* update response ([7f31987](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/7f31987e452a70a300b40eb56ae8e4b11e519b4a))


### Features

* add log service ([1f0dcfe](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/1f0dcfe79cf469254d3cf560b6bc614cca1db2e6))
* add utils ([91d8953](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/91d895390fed9ce119b68849fe2894dd0e465723))
* update config ([d4e4aa0](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/d4e4aa0631611bf48558c75a5683c5b0dbafec85))

# [1.1.0](https://bitbucket.org/fintek-digital/fintek-starter-backend/compare/v1.0.1...v1.1.0) (2024-11-06)


### Features

* add error to response ([02801b7](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/02801b720896be62af7ca3a6aa8508c47b583e52))

## [1.0.1](https://bitbucket.org/fintek-digital/fintek-starter-backend/compare/v1.0.0...v1.0.1) (2024-11-04)


### Bug Fixes

* mock server update ([06bc4e0](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/06bc4e097c82a5c5b248d0181fff5e706916f597))
* remove jwt unneeded vars ([fc4ab06](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/fc4ab06239ff1e02413229547e1fbd965fc8f935))

# 1.0.0 (2024-10-04)


### Bug Fixes

* add cors ([baf1a7a](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/baf1a7a6849dffd45395b031bfe5db5829367cfd))
* allowed origin typo ([be69733](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/be697339a9ef2977b8caef381c7d29c2bdd7627f))
* swagger routes ([4d38673](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/4d3867378798bedb7e009db135513112da03f660))


### Features

* add packages ([cce0517](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/cce05174ba7c1efbab00d5ea0f5f94d6d7f598b9))
* add scripts ([96f6d12](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/96f6d127563f3ca450c4eec373090e13e5ca6d5e))
* update packages ([eae58cc](https://bitbucket.org/fintek-digital/fintek-starter-backend/commits/eae58cce83000c842583e46fd12b23136112e2fa))

# CHANGELOG

## [0.4.1] - 04/10/2024
### changed
- updated packages

## [0.4.0] - 04/11/2023
### added
- added authentication ability

## [0.3.1] - 03/11/2023
### changed
- updated session memory store to production

## [0.3.0] - 02/11/2023
### added
- session management via memory

## [0.2.3] - 27/07/2023
### changed
- new sql connection via pool manager
- logger now uses connection pool

## [0.2.2] - 11/09/2022
### changed
- error now shows validation errors
- comtec utils functions to handle json

## [0.2.1] - 11/09/2022
### changed
- enviroment safe check is only for development
- degrade sql version to work

## [0.2.0] - 05/06/2022
### removed
- access token for secure endpoints

## [0.1.8] - 22/05/2022
### changed
- moved uuid function to util to use as global function
- removed dotenv check for development

## [0.1.0] - 01/04/2022
### added
- init project

### changed

### fixed

### removed
